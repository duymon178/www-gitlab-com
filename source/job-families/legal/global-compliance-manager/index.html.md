---
layout: job_page
title: "Global Compliance Counsel"
---

The Global Compliance Counsel at GitLab is responsible for development, management, and monitoring the GitLab’s global compliance and privacy related obligations and activities. At the direction of the Senior Director of Legal Affairs, the Global Compliance Counsel will provide guidance on compliance and privacy matters. This is a remote role, but extensive US, UK, and EU legal compliance experience is a must-have for this role.

## Responsibilities

- Facilitate the organization's compliance with applicable laws, regulations and best practices by assisting in the development, refinement and rollout of policies, and procedures. Recommend process/policy changes to ensure compliance with all regulations and assist in the documentation or training of those changes.
- Develop and implement internal employee training in cooperation with the People Operations team. Evaluating the effectiveness of the training program; and developing innovative approaches to training to encourage employee engagement.
- Assist business partners in building and maintaining compliance and privacy-related data inventories, data flows/network diagrams information, etc.
- Oversee and support the day-to-day operations of various global ethics and compliance programs and initiatives, including Code of Conduct, global training, anti-corruption due diligence program, export, gift, hospitality & travel and employee communication and engagement.
- Assist with periodic compliance and privacy-related risk assessments and controls evaluations with business partners, to verify whether business units comply with applicable regulations.
- Collaborate with department managers on solutions to overcome compliance concerns.
- Assist and answer privacy related questions from internal business partners, as well as external users; review documents for compliance with applicable compliance and privacy-related laws and regulations; review the Company’s posted compliance and privacy-related policies and in collaboration with key stakeholders update those postings as appropriate.
- Participate in new product development and provide input and guidance regarding compliance concerns relevant to proposed and current products and services
- Monitor and maintain a thorough understanding of state, federal and global laws and regulations related to GitLab’s business, monitor and analyze developing trends and changes in regulatory laws, rules, and regulations as they relate to GitLab  business activities.
- Conduct ongoing reviews of marketing, advertisements, materials, and disclosures.

## Requirements for candidate

- Minimum 5-7 years of substantial experience in the areas of legal compliance and data privacy in the US, UK, and EU.
- BA/BS and JD required.
- Thorough knowledge of and experience working with global anti-corruption and anti-slavery laws, privacy regulations (including GDPR), import and export laws, such is required.
- Ability to do in-depth research and to interpret written guidelines and documents.
- Proactive, dynamic and result driven individual with strong attention to detail.
- Ability to understand and communicate complex technical issues.
- Experience working with global teams preferred.
- Outstanding interpersonal skills, the ability to interface effectively with all business functions throughout the organization.
- Enthusiasm and "self-starter" qualities enabling him or her to manage responsibilities with an appropriate sense of urgency; the ability to function effectively and efficiently in a fast-paced & dynamic environment.
- Previous experience in a Global Start-up and remote first environment would be ideal.
- Experience with open source software a plus.
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team). The review process for this role can take a little longer than usual, but if in doubt feel free to check in with the Recruiting team at any point.

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team
* Next, candidates will be invited to schedule a first interview with our Senior Director of Legal Affairs
* Next, candidates will be invited to schedule a second interview with our CFO
* Candidates might at this point be invited to schedule with an additional C-Level team member or VP Management member
* Finally, candidates may interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
